import React, {
  createContext,
  useContext,
  useState,
  ReactNode,
  useEffect,
} from "react";

type Answer = {
  currentYear: number;
  area: string;
  position: string;
  state: string;
  exProgrammingLanguage: string;
  level: number;
  demand: number;
  exFramework: number;
  exOtherFrameworks: number;
  exIdea: number;
  exOtherIdeas: number;
  exGit: number;
  exJira: number;
  exConfluence: number;
  exGitlab: number;
  exGithub: number;
  exAzureDevops: number;
  exCiCd: number;
  exCloud: number;
  exOtherClouds: number;
  exDatabase: number;
  exOtherDatabases: number;
  exTdd: number;
  exUnitTest: number;
  exIntegrationTest: number;
  exScrum: number;
  exKanban: number;
  workedAiProject: number;
  aiSkills: number;
  employmentType: number;
  finalOffer?: number;
};

type QuestionContextType = {
  answer: Answer | undefined;
  updateAnswer: (parameter: keyof Answer, value: any) => void;
  clearAnswer: () => void;
};

const questionContext = createContext<QuestionContextType | undefined>(
  undefined
);

export const useQuestionContext = () => {
  const context = useContext(questionContext);
  if (!context) {
    throw new Error(
      "useQuestionContext must be used within a QuestionProvider"
    );
  }
  return context;
};

type QuestionProviderProps = {
  children: ReactNode;
};

export const QuestionProvider: React.FC<QuestionProviderProps> = ({
  children,
}) => {
  const [answer, setAnswer] = useState<Answer>({} as Answer);

  useEffect(() => {
    const storedAnswer = localStorage.getItem("answer");
    if (storedAnswer) {
      setAnswer(JSON.parse(storedAnswer));
    }
  }, []);

  const updateAnswer = (parameter: keyof Answer, value: any) => {
    if (!answer) return;
    const newAnswer = { ...answer, [parameter]: value };
    setAnswer(newAnswer);
    localStorage.setItem("answer", JSON.stringify(newAnswer)); // Save to localStorage
  };

  const clearAnswer = () => {
    setAnswer({} as Answer);
    localStorage.removeItem("answer");
  };

  return (
    <questionContext.Provider value={{ answer, updateAnswer, clearAnswer }}>
      {children}
    </questionContext.Provider>
  );
};
