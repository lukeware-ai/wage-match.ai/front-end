import { memo } from "react"


type Option = {
    value: string;
    label: string;
}

type Props = {
    selectOption: string;
    options: Option[];
    onChange: (e: string | number | undefined) => void
}


const SelectButton = memo(({ selectOption, options, onChange }: Props) => {
    return (
        <div className="relative inline-block w-full">
            <select
                value={selectOption}
                onChange={(e) => onChange(Number(e.target.value))}
                className="appearance-none bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-gray-500 focus:border-gray-500 w-full p-2">
                <option value={0} selected>Choose a Level</option>
                {options.map((it) => <option value={it.value}>{it.label}</option>)}
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                </svg>
            </div>
        </div>
    )
});

export { SelectButton }