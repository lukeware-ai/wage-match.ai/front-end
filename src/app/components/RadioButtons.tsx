import React, { memo, useState, ChangeEvent, useEffect } from "react";

type Option = {
    value: string;
    label: string;
}

type RadioButtonsProps = {
    options: Option[];
    selectedValue?: string;
    onChange: (value: string) => void; // Alterado para aceitar qualquer tipo
}

const RadioButtons: React.FC<RadioButtonsProps> = memo(({ options, selectedValue, onChange }: RadioButtonsProps) => {
    const [selected, setSelected] = useState<string>(options[0]?.value);


    useEffect(() => {
        if (selectedValue) {
            setSelected(selectedValue)
        }
    }, [selectedValue])

    return (
        <div className="flex flex-row justify-between items-center">
            {options.map((option) => (
                <div key={option.value}
                    className="flex items-center ps-4 border border-gray-200 rounded dark:border-gray-300 active:border-gray-400 w-[12rem] h-[3rem]"
                    onClick={() => {
                        setSelected(option.value);
                        onChange(option.value)
                    }}>
                    <input
                        id={`bordered-radio-${option.value}`}
                        type="radio"
                        value={option.value}
                        name="bordered-radio"
                        checked={selected === option.value}
                        onChange={(e) => {
                            setSelected(option.value);
                            onChange(option.value)
                        }}
                        className="w-6 h-6 border-gray-300"
                    />

                    <span className="w-full py-4 ms-2 text-gray-500 uppercase">
                        {option.label}
                    </span>
                </div>
            ))}
        </div>
    );
});

export { RadioButtons };
