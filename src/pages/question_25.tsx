import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exIntegrationTest", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exIntegrationTest) {
        setValue(answer.exIntegrationTest);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Experience with Integration Test"
      question="Does this professional have advanced skills with Integration Test?"
      onPrevious={() => router.push("/question_24")}
      onNext={() => onGoTo("/question_26")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
