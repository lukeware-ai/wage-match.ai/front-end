import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question8: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exOtherFrameworks", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exOtherFrameworks) {
        setValue(answer.exOtherFrameworks);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Mastery of Other Types of Frameworks"
      question="Does the candidate master more than one type of framework?"
      onPrevious={() => router.push("/question_7")}
      onNext={() => onGoTo("/question_9")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question8;
