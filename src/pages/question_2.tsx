// src/app/questions/Question1.tsx

import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";

import toast from "react-hot-toast";
import { camelCase, removeGeral } from "@/lib/caracters-util";

const Question2: React.FC = () => {
  const [value, setValue] = useState<string | undefined>("");
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    if (value) {
      updateAnswer("position", removeGeral(value));
      router.push(route);
    } else {
      toast.error("Oops, I think you didn't type, huh!?", {
        position: 'top-right',
        duration: 4000,
        icon: '⚠️',
        style: {
          background: '#333',
          color: '#fff',
        },
      });
    }
  };

  useEffect(() => {
    if (answer) {
      if (answer.position) {
        setValue(camelCase(answer.position));
      }
    }
  }, [answer]);

  return (
    <Question
      title="Professional Position"
      question="What will be the candidate's position? Do you know which one it is?"
      onPrevious={() => router.push("/question_1")}
      onNext={() => onGoTo("/question_3")}
    >
      <span className="text-gray-500" >ex. Backend Software Developer, Front-end Software Developer</span>
      <input
        type="text"
        placeholder="Enter Professional Position"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
        }}
        className="w-full h-10 px-3 mt-2 text-base placeholder-gray-500 border rounded-lg focus:outline-none focus:border-gray-400 text-gray-500"
      />
    </Question>
  );
};

export default Question2;
