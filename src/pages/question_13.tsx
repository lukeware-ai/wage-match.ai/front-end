import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question13: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exConfluence", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exConfluence) {
        setValue(answer.exConfluence);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Confluence Mastery"
      question="Can you tell me if he masters Confluence?"
      onPrevious={() => router.push("/question_12")}
      onNext={() => onGoTo("/question_14")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question13;
