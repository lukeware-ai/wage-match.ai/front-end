import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question9: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exIdea", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exIdea) {
        setValue(answer.exIdea);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Mastery of an IDE"
      question="Does the professional master at least one IDE?"
      onPrevious={() => router.push("/question_8")}
      onNext={() => onGoTo("/question_10")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question9;
