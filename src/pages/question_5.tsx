import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";

import toast from "react-hot-toast";
import { SelectButton } from "@/app/components/SelectButton";

const Question5: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    if (value > 0) {
      updateAnswer("level", value);
      router.push(route);
    } else {
      toast.error("Oops, I think you didn't select an option, huh!?", {
        position: 'top-right',
        duration: 4000,
        icon: '⚠️',
        style: {
          background: '#333',
          color: '#fff',
        },
      });
    }
  };

  useEffect(() => {
    if (answer) {
      if (answer.level) {
        setValue(answer.level);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Candidate Level"
      question="Can you inform me about the candidate's level?"
      onPrevious={() => router.push("/question_4")}
      onNext={() => onGoTo("/question_6")}
    >
      <div className="mb-2">
        <span className="text-gray-500" >ex. Intern, Junior, Mid-Level, etc.</span>
      </div>
      <SelectButton
        options={[
          { label: "Junior", value: "1" },
          { label: "Mid-Level", value: "2" },
          { label: "Senior", value: "3" }
        ]}
        selectOption={String(value)}
        onChange={(e) => setValue(Number(e))}
      />
    </Question>
  );
};

export default Question5;
