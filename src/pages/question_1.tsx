// src/app/questions/Question1.tsx

import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";

import toast from "react-hot-toast";
import { camelCase, removeGeral } from "@/lib/caracters-util";

const Question1: React.FC = () => {
  const [value, setValue] = useState<string | undefined>("");
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    if (value) {
      updateAnswer("area", removeGeral(value));
      router.push(route);
    } else {
      toast.error("Oops, I think you didn't type, huh!?", {
        position: 'top-right',
        duration: 4000,
        icon: '⚠️',
        style: {
          background: '#333',
          color: '#fff',
        },
      });
    }
  };

  useEffect(() => {
    if (answer) {
      if (answer.area) {
        setValue(camelCase(answer.area));
      }
    }
  }, [answer]);

  return (
    <Question
      title="Field of Work"
      question="Can you tell me in which field the professional will work?"
      onNext={() => onGoTo("/question_2")}
    >
      <span className="text-gray-500" >ex. Software Development, Software Engineering</span>
      <input
        type="text"
        placeholder="Enter Field of Work"
        value={value}
        onChange={(e) => {
          setValue(e.target.value);
        }}
        className="w-full h-10 px-3 mt-2 text-base placeholder-gray-500 border rounded-lg focus:outline-none focus:border-gray-400 text-gray-500"
      />
    </Question>
  );
};

export default Question1;
