import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("workedAiProject", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.workedAiProject) {
        setValue(answer.workedAiProject);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Experience with AI"
      question="Has the candidate ever worked on a 100% AI project?"
      onPrevious={() => router.push("/question_27")}
      onNext={() => onGoTo("/question_29")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
