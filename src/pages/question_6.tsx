import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";

import toast from "react-hot-toast";
import { SelectButton } from "@/app/components/SelectButton";

const Question6: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    if (value > 0) {
      updateAnswer("demand", value);
      router.push(route);
    } else {
      toast.error("Oops, I think you didn't select an option, huh!?", {
        position: 'top-right',
        duration: 4000,
        icon: '⚠️',
        style: {
          background: '#333',
          color: '#fff',
        },
      });
    }
  };

  useEffect(() => {
    if (answer) {
      if (answer.demand) {
        setValue(answer.demand);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Demand Level"
      question="Now, I need to know the demand level for this candidate."
      onPrevious={() => router.push("/question_5")}
      onNext={() => onGoTo("/question_7")}
    >
      <div className="mb-2 flex flex-col">
        <span className="text-gray-500" >ex.</span>
        <span className="text-gray-500" >New candidate - low demand level</span>
        <span className="text-gray-500" >Experienced candidate - moderate demand level</span>
        <span className="text-gray-500" >Highly experienced candidate - high demand level</span>
      </div>
  
      <SelectButton
        options={[
          { label: "Low", value: "1" },
          { label: "Moderate", value: "2" },
          { label: "High", value: "3" },
        ]}
        selectOption={String(value)}
        onChange={(e) => setValue(Number(e))}
      />

    </Question>
  );
};

export default Question6;
