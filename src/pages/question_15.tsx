import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question15: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exGithub", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exGithub) {
        setValue(answer.exGithub);
      }
    }
  }, [answer]);

  return (
    <Question
      title="GitHub Mastery"
      question="Does the candidate master GitHub?"
      onPrevious={() => router.push("/question_14")}
      onNext={() => onGoTo("/question_16")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question15;
