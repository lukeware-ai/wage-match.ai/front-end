import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("employmentType", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.employmentType) {
        setValue(answer.employmentType);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Works as CLT or PJ"
      question="Are you planning to hire him as CLT or PJ?"
      onPrevious={() => router.push("/question_29")}
      onNext={() => onGoTo("/completion")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "CLT", value: "0" },
          { label: "PJ", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
