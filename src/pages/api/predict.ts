import { NextApiRequest, NextApiResponse } from "next";

type Answer = {
  currentYear: number;
  area: string;
  position: string;
  state: string;
  exProgrammingLanguage: string;
  level: number;
  demand: number;
  exFramework: number;
  exOtherFrameworks: number;
  exIdea: number;
  exOtherIdeas: number;
  exGit: number;
  exJira: number;
  exConfluence: number;
  exGitlab: number;
  exGithub: number;
  exAzureDevops: number;
  exCiCd: number;
  exCloud: number;
  exOtherClouds: number;
  exDatabase: number;
  exOtherDatabases: number;
  exTdd: number;
  exUnitTest: number;
  exIntegrationTest: number;
  exScrum: number;
  exKanban: number;
  workedAiProject: number;
  aiSkills: number;
  employmentType: number;
};

type Request = {
  ano_atual: number;
  area: string;
  cargo: string;
  estado: string;
  ex_liguagem_desenv: string;
  nivel: number;
  demanda: number;
  ex_framework_1_0: number;
  ex_outros_frameworks_1_0: number;
  ex_idea_1_0: number;
  ex_outras_ideas_1_0: number;
  ex_git_1_0: number;
  ex_jira_1_0: number;
  ex_confluence_1_0: number;
  ex_gitlab_1_0: number;
  ex_github_1_0: number;
  ex_azure_devops_1_0: number;
  ex_ci_cd_1_0: number;
  ex_cloud_1_0: number;
  ex_outras_clouds_1_0: number;
  ex_database_1_0: number;
  ex_outros_database_1_0: number;
  ex_tdd_1_0: number;
  ex_unit_test_1_0: number;
  ex_integration_test_1_0: number;
  ex_scrum_1_0: number;
  ex_kanban_1_0: number;
  atuou_projeto_ai_1_0: number;
  domina_ai_1_0: number;
  regime_contratacao_1_0: number;
};

function convertToRequest(answer: Answer): Request {
  return {
    ano_atual: answer.currentYear,
    area: answer.area,
    cargo: answer.position,
    estado: answer.state,
    ex_liguagem_desenv: answer.exProgrammingLanguage,
    nivel: answer.level,
    demanda: answer.demand,
    ex_framework_1_0: answer.exFramework,
    ex_outros_frameworks_1_0: answer.exOtherFrameworks,
    ex_idea_1_0: answer.exIdea,
    ex_outras_ideas_1_0: answer.exOtherIdeas,
    ex_git_1_0: answer.exGit,
    ex_jira_1_0: answer.exJira,
    ex_confluence_1_0: answer.exConfluence,
    ex_gitlab_1_0: answer.exGitlab,
    ex_github_1_0: answer.exGithub,
    ex_azure_devops_1_0: answer.exAzureDevops,
    ex_ci_cd_1_0: answer.exCiCd,
    ex_cloud_1_0: answer.exCloud,
    ex_outras_clouds_1_0: answer.exOtherClouds,
    ex_database_1_0: answer.exDatabase,
    ex_outros_database_1_0: answer.exOtherDatabases,
    ex_tdd_1_0: answer.exTdd,
    ex_unit_test_1_0: answer.exUnitTest,
    ex_integration_test_1_0: answer.exIntegrationTest,
    ex_scrum_1_0: answer.exScrum,
    ex_kanban_1_0: answer.exKanban,
    atuou_projeto_ai_1_0: answer.workedAiProject,
    domina_ai_1_0: answer.aiSkills,
    regime_contratacao_1_0: answer.employmentType,
  };
}


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    return res
      .status(405)
      .json({ status: "error", message: "Method Not Allowed" });
  }

  try {
    const answer = req.body as Answer;
    const request = convertToRequest(answer);

    const data_request = JSON.stringify(request);

    const response = await fetch(
      "http://192.168.15.66:8890/wage-match-ai/api/v1/predict",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: data_request,
      }
    );

    if (!response.ok) {
      throw new Error(
        `Failed to fetch data from prediction service: ${response}`
      );
    }

    const data = await response.json();
    res.status(200).json(data);
  } catch (error: any) {
    res.status(500).json({ status: "error", message: error.message });
  }
}
