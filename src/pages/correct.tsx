import "@/app/globals.css";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { FcHighPriority, FcOk } from "react-icons/fc";
import { GoHomeFill } from "react-icons/go";
import toast from "react-hot-toast";

const Question1: React.FC = () => {
  const router = useRouter();
  const { clearAnswer, answer } = useQuestionContext();

  const formatCurrency = (
    value: number,
    locale = "pt-BR",
    currency = "BRL"
  ) => {
    return new Intl.NumberFormat(locale, {
      style: "currency",
      currency: currency,
    }).format(value);
  };

  const [value, setValue] = useState<number>(2000);
  const [valueFormat, setValueFormat] = useState<string>(formatCurrency(2000));
  const [status, setStatus] = useState<boolean>(false);

  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch("/api/check_health");
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();
  }, []);
  useEffect(() => {
    if (answer) {
      if (answer.finalOffer) {
        setValue(answer.finalOffer);
        setValueFormat(formatCurrency(answer.finalOffer));
      }
    }
  }, [answer]);

  const fetchData = async () => {
    if (answer) {
      try {
        const request = { ...answer };
        request.currentYear = 2023;
        request.level = answer.level - 1;
        request.demand = answer.demand - 1;
        request.finalOffer = value

        const response = await fetch("/api/instance", {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(request),
        });

        if (!response.ok) {
          toast.error("Oops, I think there was a mistake here, right!?", {
            position: "top-right",
            duration: 4000,
            icon: "⚠️",
            style: {
              background: "#333",
              color: "#fff",
            },
          });
          throw new Error("Failed to fetch prediction");
        }

        router.push("/thank_you");
        clearAnswer();
      } catch (error: any) {
        toast.error("Oops, I think there was a mistake here, right!?", {
          position: "top-right",
          duration: 4000,
          icon: "⚠️",
          style: {
            background: "#333",
            color: "#fff",
          },
        });
        console.error(error);
      }
    }
  };

  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <GoHomeFill
            className="w-8 h-8 text-gray-700"
            onClick={() => router.push("/")}
          />
        </div>
      </div>
      <div className="relative">
        <div className="absolute top-0 right-0 m-4">
          {status === true && <FcOk className="w-5 h-5 text-gray-700" />}
          {status === false && (
            <FcHighPriority className="w-5 h-5 text-gray-700" />
          )}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-2xl">
          <h1 className="text-base sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
            Updating New Values
          </h1>
          {/* <h2 className="text-xl font-light mb-4 text-gray-700 text-justify">Com base nas respostas, o salário sugerido a ser proposto ao candidato é de:</h2> */}
          <h2 className="text-[17px] sm:text-xl font-light mb-4 text-gray-700 text-justify">
            I would like to apologize for providing the wrong salary offer.
            Let's adjust this together to improve. What value do you believe is
            better based on this data?
          </h2>
          <div className="mb-4 text-purple-700 font-medium text-3xl text-center">
            {valueFormat}
          </div>
          <input
            type="range"
            min={2000}
            max={40000}
            value={value}
            onChange={(e) => {
              setValueFormat(formatCurrency(Number(e.target.value)));
              setValue(Number(e.target.value));
            }}
            className="w-full h-1 mb-6 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm dark:bg-gray-500"
          />
          <div className="flex justify-between">
            <p className="text-xl font-light mb-4 text-gray-700 text-justify">
              R$ 2.000,00
            </p>
            <p className="text-xl font-light mb-4 text-gray-700 text-justify">
              R$ 40.000,00
            </p>
          </div>
          <div className="flex flex-col md:flex-row justify-between">
            <button
              className="bg-gray-300 text-white py-2 px-10 rounded hover:bg-gray-400"
              onClick={() => {
                router.push("/completion");
              }}
            >
              Back
            </button>
            <button
              className="bg-purple-600 text-white py-2 px-10 rounded hover:bg-purple-700 my-2 md:my-0"
              onClick={() => fetchData()}
            >
              Register
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Question1;
