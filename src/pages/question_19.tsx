import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exOtherClouds", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exOtherClouds) {
        setValue(answer.exOtherClouds);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Mastery in Other Clouds"
      question="Does the candidate have mastery in other cloud platforms?"
      onPrevious={() => router.push("/question_18")}
      onNext={() => onGoTo("/question_20")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
