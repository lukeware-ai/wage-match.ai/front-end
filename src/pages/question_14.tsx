import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question14: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exGitlab", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exGitlab) {
        setValue(answer.exGitlab);
      }
    }
  }, [answer]);

  return (
    <Question
      title="GitLab Mastery"
      question="Does the candidate master GitLab?"
      onPrevious={() => router.push("/question_13")}
      onNext={() => onGoTo("/question_15")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question14;
