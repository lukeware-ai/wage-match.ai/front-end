import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exDatabase", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exDatabase) {
        setValue(answer.exDatabase);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Database Mastery"
      question="Does the professional have advanced experience with any database?"
      onPrevious={() => router.push("/question_19")}
      onNext={() => onGoTo("/question_21")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
