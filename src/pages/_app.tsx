import React from 'react';
import { AppProps } from 'next/app';
import { QuestionProvider } from '@/contexts/QuestionContext';
import { Toaster } from 'react-hot-toast';
import "@/app/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QuestionProvider>
      <Toaster position="bottom-center" />
      <Component {...pageProps} />
    </QuestionProvider>
  );
}

export default MyApp;
