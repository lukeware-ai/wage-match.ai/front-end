import "@/app/globals.css";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { FcHighPriority, FcOk } from "react-icons/fc";
import { GoHomeFill } from "react-icons/go";

const Question1: React.FC = () => {
  const router = useRouter();

  const { answer, clearAnswer, updateAnswer } = useQuestionContext();
  const [value, setValue] = useState<string>();
  const [status, setStatus] = useState<boolean>(false);
  const [newInstance, setNewInstance] = useState<boolean>(false);

  const formatCurrency = (
    value: number,
    locale = "pt-BR",
    currency = "BRL"
  ) => {
    return new Intl.NumberFormat(locale, {
      style: "currency",
      currency: currency,
    }).format(value);
  };

  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch("/api/check_health");
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      if (answer) {
        try {
          const request  = {...answer}
          request.currentYear = 2023;
          request.level = answer.level - 1;
          request.demand = answer.demand - 1;
          const response = await fetch("/api/predict", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(request),
          });

          if (!response.ok) {
            throw new Error("Failed to fetch prediction");
          }

          const data = await response.json();
          if (data.data.new_instance) {
            setNewInstance(data.data.new_instance !== undefined);
          }
          setValue(formatCurrency(data.data.label));
          updateAnswer("finalOffer", Number(data.data.label));
        } catch (error: any) {
          console.error(error);
        }
      }
    };

    fetchData();
  }, [answer]);

  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <GoHomeFill
            className="w-8 h-8 text-gray-700"
            onClick={() => router.push("/")}
          />
        </div>
      </div>
      <div className="relative">
        <div className="absolute top-0 right-0 m-4">
          {status === true && <FcOk className="w-5 h-5 text-gray-700" />}
          {status === false && (
            <FcHighPriority className="w-5 h-5 text-gray-700" />
          )}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-md">
          <h1 className="text-[17px] sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
            CONGRATULATIONS, WE'VE REACHED THE END!
          </h1>
          <h2 className="text-base sm:text-xl font-light mb-4 text-gray-700 text-justify">
            Thank you very much for providing me with all this information. I
            believe we should make an offer with this value. What do you think?
          </h2>
          {newInstance && (
            <h5 className="text-base sm:text-xl font-light mb-4 text-gray-500 text-justify">
              These pieces of information are new to me, do you know if they're
              correct?
            </h5>
          )}
          <div className="mb-4 text-purple-700 font-medium text-3xl text-center">
            {value}
          </div>
          <div className="flex flex-col md:flex-row justify-between">
            <button
              className="bg-gray-300 text-white py-2 px-4 md:px-10 rounded hover:bg-gray-400"
              onClick={() => {
                router.push("/question_30");
              }}
            >
              Back
            </button>
            <button
              className="bg-fuchsia-500 text-white py-2 px-4 md:px-10 rounded hover:bg-fuchsia-700 my-2 md:my-0"
              onClick={() => router.push("/correct")}
            >
              Correct
            </button>
            <button
              className="bg-purple-600 text-white py-2 px-4 md:px-10 rounded hover:bg-purple-700"
              onClick={() => {
                clearAnswer();
                router.push("/");
              }}
            >
              Finish
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Question1;
