import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question11: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exGit", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exGit) {
        setValue(answer.exGit);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Git Mastery"
      question="Can you tell me if he masters Git?"
      onPrevious={() => router.push("/question_10")}
      onNext={() => onGoTo("/question_12")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question11;
