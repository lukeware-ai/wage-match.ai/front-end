import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question16: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exOtherDatabases", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exOtherDatabases) {
        setValue(answer.exOtherDatabases);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Mastery in Other Databases"
      question="And with other databases, does he also have experience?"
      onPrevious={() => router.push("/question_20")}
      onNext={() => onGoTo("/question_23")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question16;
