import "@/app/globals.css";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { RadioButtons } from "@/app/components/RadioButtons";

const Question10: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  const router = useRouter();

  const { answer, updateAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    updateAnswer("exOtherIdeas", value);
    router.push(route);
  };

  useEffect(() => {
    if (answer) {
      if (answer.exOtherIdeas) {
        setValue(answer.exOtherIdeas);
      }
    }
  }, [answer]);

  return (
    <Question
      title="Mastery of Other IDEs"
      question="Does he have complete mastery in other IDEs?"
      onPrevious={() => router.push("/question_9")}
      onNext={() => onGoTo("/question_11")}
    >
      <RadioButtons
        selectedValue={String(value)}
        onChange={(e) => setValue(Number(e))}
        options={[
          { label: "No", value: "0" },
          { label: "Yes", value: "1" }
        ]} />
    </Question>
  );
};

export default Question10;
