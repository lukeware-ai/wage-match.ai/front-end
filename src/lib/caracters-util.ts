const removeAccents = (value: string) => {
  return value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};

const remove = (value: string) => {
  return value.replace(/[^A-Za-z\s]/g, "").replace(/\s/g, "_");
};

const removeGeral = (value: string) => {
  const newValue = value.trim();
  const valueWhioutAccents = removeAccents(newValue);
  return remove(valueWhioutAccents).toLocaleLowerCase();
};

const camelCase = (value: string) => {
  return value
    .replace(/_/g, " ")
    .replace(/\b\w/g, (char) => char.toUpperCase());
};

export { removeAccents, remove, removeGeral, camelCase };
